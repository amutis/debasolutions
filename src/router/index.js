import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/home/Home'
import Index from '@/components/home/Index'
import Register from '@/components/home/Register'
import Blog from '@/components/home/Blog'
import AboutUs from '@/components/home/AboutUs'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '',
      name: 'Home',
      component: Home,
      children: [
        {
          path: '/',
          name: 'Index',
          component: Index
        },
        {
          path: 'register',
          name: 'Register',
          component: Register
        },
        {
          path: 'blog',
          name: 'Blog',
          component: Blog
        },
        {
          path: 'about',
          name: 'AboutUs',
          component: AboutUs
        }
      ]
    }
  ],
  mode: 'history'
})
